variable "cidr_list" {
  default = {
    us-east-1 = "172.20.0.0/16"
    us-east-2 = "172.21.0.0/16"    
  }
}

variable "cidr_subnet_list" {
  default = {
    "172.20.0.0/16" = "172.20.1.0/24" 
    "172.21.0.0/16" = "172.21.1.0/24"   
  }
}

variable "ami_list" {
  default = {
    us-east-2 = "ami-089fe97bc00bff7cc" #"ami-0071048c60844169f" # Debian 10 ami    
  }
}

variable "database_ports" {
  type    = list(number)
  default = [22,5432]
}

variable "availability_zone" {
  type = string  
  default = "us-east-2"
}

variable "availability_zone2" {
  type = string  
  default = "us-east-1"
}
