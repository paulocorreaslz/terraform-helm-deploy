data "template_file" "ansible_hosts" {
  template = file("../../ansible/templates/hosts")
  vars = {
    postgresql_host             = module.vm_database["private_ip"]
    postgresql_port             = "5132"
    postgresql_db_name          = "database"
    postgresql_db_user          = "username1"
    postgresql_db_user_password = "password1"
    
    }
}
resource "null_resource" "ansible_hosts_output" {
  triggers = {
    template = data.template_file.ansible_hosts.rendered
  }
  provisioner "local-exec" {
    command = "echo \"${data.template_file.ansible_hosts.rendered}\" > ${join("/", ["../../ansible/templates", "hosts"])}"
  }
}

output "vpc_id" {
  value = aws_vpc.prod_vpc_main.id
}
output "route_table_id" {
  value = aws_route_table.prod_route_table_main.id
}

output "vpc_cidr_block" {
  value = aws_vpc.prod_vpc_main.cidr_block
}

output "gateway_id" {
  value = aws_internet_gateway.prod_gateway.id
}

output "subnet_id" {
  value = aws_subnet.prod_subnet_main.id
}

# ------------------------

output "database_data" {
  value = {
    "private_dns" : module.vm_database.private_dns
    "private_ip" : module.vm_database.private_ip,
    "public_dns" : module.vm_database.public_dns,
    "public_ip" : module.vm_database.public_ip,
  }
}