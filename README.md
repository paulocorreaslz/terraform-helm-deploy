# Infra Technical Challenge
Hi, we're glad you chose to go through our technical challenge. It will help us assess your skills and show us how you solve problems. It will also serve as a starting point for a conversation about our project and what we are trying to achieve.

We value your time, so please don't take more than 3 or 4 hours to work on the challenge. We are also not expecting a perfect solution or for you to complete all the tasks. Work on the ones you are more excited about.

Good luck :)

## Table of Contents
- [Recommendations](#pre-requisites)
- [Objectives](#objectives)

## Recommendations
- These are the environments and tool our team currently uses. We recommend that you also use them for the challenge, but feel free to use the ones you are comfortable with.

    | Tool | Recommendation | Link |
    | --- | --- | --- |
    | Cloud Environment | AWS | [Create an account](https://aws.amazon.com/free) |
    | CI/CD | Gitlab CI | [Quick start](https://docs.gitlab.com/ee/ci/quick_start/) |
    | Infrastructure as Code | Terraform | [Introduction](https://www.terraform.io/intro/index.html) |
    | Kubernetes | EKS | [Overview](https://aws.amazon.com/eks) |
    | K8s deployments | Helm | [Quick start](https://helm.sh/docs/intro/quickstart/) |

- We know deploying and keeping a cloud environment can be costly. We suggest that you at least test your code by deploying it on your preferred cloud provider, but feel free to destroy everything afterwards.
- Avoid simply copying an example from the documentation. We know this works, and we do this often ourselves, but it makes it difficult for us to assess your experience and skills. Do something a little different, or modify it in a way that shows us your style.
- Provide clear documentation and instructions on your README file
- Work on a branch and create a merge request

## Objectives
### Task 1
You get this repository handed to you by a quite junior Python developer from your team. Of course, the documentation is not ready yet, but surely enough, this service needs to go to production at the end of this business day.

1. Write a CI pipeline to build and push a Docker image with the Python "Hello World" API. Its in the .gitlab-ci.yml file.
1. Which pipeline steps would you recommend to the developer and why? - Configure, Build, Test, Deploy at least.
1. Write the deployment or resource files using your tool of choice to deploy the API on a Kubernetes cluster. I didn't finish the deployment because I didn't finish the helm configuration.

### Task 2
Our team is responsible for deploying and maintaing the Kubernetes clusters used by all the developers. Your next task is to deploy a Kubernetes cluster on a cloud environment using Infrastructure as Code.

1. Write the code to deploy a VPC (or equivalent), a Kubernetes cluster, and a PostgreSQL database. With terraform a virtual machine was created with the postgresql database and a Kubernetes EKS cluster.
1. What security concerns do you have, and what would you recommend to secure this environment? I recommend keeping it in a secure environment, behind a vpn and leaving only the necessary ports for each system open, in addition to ports for ssh access only.
1. What method would you recommend to right-size the deployment? Can it scale up during peak load? using the horizontal pod autoscale it can autoscale during a peak load.

### Task 3
Your last task is to deploy the "Hello World" API on the Kubernetes cluster, and make it accessible on the Internet.

1. Deploy the API on the cluster, preferably using the CI pipeline. Didnt finish
1. Enable access through an Ingress controller. Didnt finish
1. Enable HTTPS, either using the cloud provider or managed inside the cluster. Didnt finish
1. Register an FQDN that resolves to the deployed API. Didnt finish
1. What method would you recommend for scaling the API deployment based on load? Didnt finish


To create the environment, with the aws cli and a user with privilegies, you can simple run:

`AWS_DEFAULT_REGION="us-east-2" terraform plan --out plan`

And

`AWS_DEFAULT_REGION="us-east-2" terraform apply plan`

to get your EKS cluster and your database VM.