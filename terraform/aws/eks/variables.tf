# variable "subnets" {
# type    = list(string)
# }

variable "availability_zone" {
  type = string  
  default = "us-east-2b"
}

variable "availability_zone2" {
  type = string  
  default = "us-east-2c"
}

variable "cidr_list" {
  default = {
    us-east-2b = "172.20.0.0/16"
    us-east-2c = "172.21.0.0/16"    
  }
}

variable "cidr_subnet_list" {
  default = {
    "172.20.0.0/16" = "172.20.1.0/24"
    "172.21.1.0/16" = "172.21.1.0/24"
  }
}