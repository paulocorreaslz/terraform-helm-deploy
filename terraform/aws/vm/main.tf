terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
  alias  = "us-2"
}

resource "aws_instance" "instance" {
  ami             = var.ami  
  instance_type   = "a1.large"
  security_groups = [var.security_group]
  subnet_id       = var.current_subnet_id
  tags = {
    Name = "${var.vm_name}"
  }
}
