terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27.0"      
    }
  }
}

provider "aws" {
  region = "us-east-2a"
  alias  = "us-2"
}

resource "aws_vpc" "prod_vpc_main" {
  cidr_block           = "172.20.0.0/16"
  enable_dns_support   = "true" # internal website name
  enable_dns_hostnames = "true" # internal host name

  tags = {
    Name = "production_vpc"
  }
}

module "security" {
  source            = "./security"
  database_ports    = var.database_ports  
  current_vpc_id    = aws_vpc.prod_vpc_main.id
}

resource "aws_internet_gateway" "prod_gateway" {
  vpc_id = aws_vpc.prod_vpc_main.id

  tags = {
    Name = "production_gateway"
  }
}

resource "aws_subnet" "prod_subnet_main" {
  vpc_id                  = aws_vpc.prod_vpc_main.id
  cidr_block              = var.cidr_subnet_list[aws_vpc.prod_vpc_main.cidr_block]
  map_public_ip_on_launch = "true" # it makes this a public subnet
  availability_zone       = "us-east-2b"

  tags = {
    Name = "production_subnet"
  }
}

resource "aws_route_table" "prod_route_table_main" {
  vpc_id = aws_vpc.prod_vpc_main.id

  route {
    cidr_block = "0.0.0.0/0" # associated subnet can reach everywhere
    gateway_id = aws_internet_gateway.prod_gateway.id
  }
  tags = {
    Name = "production_route_table"
  }
}

resource "aws_route_table_association" "requesting_route_table_subnet_root" {
  subnet_id      = aws_subnet.prod_subnet_main.id
  route_table_id = aws_route_table.prod_route_table_main.id
}


module "vm_database" {
  source            = "./vm"
  ami               = "ami-0071048c60844169f"
  region            = "us-east-2"
  security_group    = module.security.database_group_id
  vm_name           = "vm_database"
  current_vpc_id    = aws_vpc.prod_vpc_main.id
  current_subnet_id = aws_subnet.prod_subnet_main.id  
}

module "eks-cluster" {
  source            = "./eks"    
  }

