
variable "database_ports" {
  type    = list(number)
  default = [22,5432]
}

variable "current_vpc_id" {
  type = string
}

