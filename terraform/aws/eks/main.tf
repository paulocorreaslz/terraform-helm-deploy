terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
  alias  = "us-2"
}

resource "aws_vpc" "eks_vpc" {
  cidr_block           = "172.20.0.0/16"
  enable_dns_support   = "true" # internal website name
  enable_dns_hostnames = "true" # internal host name

  tags = {
    Name = "production_vpc"
  }
}

resource "aws_subnet" "prod_subnet" {
  vpc_id                  = aws_vpc.eks_vpc.id
  cidr_block              = "172.20.0.0/24"
  map_public_ip_on_launch = "true" # it makes this a public subnet
  availability_zone       = "us-east-2c"

  tags = {
    Name = "production_subnet"
  }
}

resource "aws_subnet" "prod_subnet2" {
  vpc_id                  = aws_vpc.eks_vpc.id
  cidr_block              = "172.20.1.0/24"
  map_public_ip_on_launch = "true" # it makes this a public subnet
  availability_zone       = "us-east-2b"

  tags = {
    Name = "production_subnet2 eks"
  }
}

resource "aws_internet_gateway" "prod_gateway_eks" {
  vpc_id = aws_vpc.eks_vpc.id

  tags = {
    Name = "production_gateway eks"
  }
}

resource "aws_route_table" "prod_route_table_eks" {
  vpc_id = aws_vpc.eks_vpc.id

  route {
    cidr_block = "0.0.0.0/0" # associated subnet can reach everywhere
    gateway_id = aws_internet_gateway.prod_gateway_eks.id
  }
  tags = {
    Name = "production_route_table"
  }
}

resource "aws_route_table" "prod_route_table_eks2" {
  vpc_id = aws_vpc.eks_vpc.id

  route {
    cidr_block = "0.0.0.0/0" # associated subnet can reach everywhere
    gateway_id = aws_internet_gateway.prod_gateway_eks.id
  }
  tags = {
    Name = "production_route_table"
  }
}

resource "aws_route_table_association" "requesting_route_table_subnet1_root" {
  subnet_id      = aws_subnet.prod_subnet.id
  route_table_id = aws_route_table.prod_route_table_eks.id
}

resource "aws_route_table_association" "requesting_route_table_subnet2_root" {
  subnet_id      = aws_subnet.prod_subnet2.id
  route_table_id = aws_route_table.prod_route_table_eks2.id
}

resource "aws_eks_cluster" "cluster-eks" {
  name     = "cluster-eks"
  role_arn = aws_iam_role.iam-role.arn

  vpc_config {
    subnet_ids = [aws_subnet.prod_subnet.id,aws_subnet.prod_subnet2.id]
  }

  depends_on = [
    aws_iam_role_policy_attachment.policy-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.policy-AmazonEKSVPCResourceController,
  ]
}

resource "aws_eks_node_group" "eks-node" {
  cluster_name    = aws_eks_cluster.cluster-eks.name
  node_group_name = "eks-node"
  node_role_arn   = aws_iam_role.iam-role.arn
  subnet_ids      = [aws_subnet.prod_subnet.id,aws_subnet.prod_subnet2.id]

  scaling_config {
    desired_size = 2
    max_size     = 4
    min_size     = 1
  }

  depends_on = [
    aws_iam_role_policy_attachment.policy-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.policy-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.policy-AmazonEC2ContainerRegistryReadOnly,
  ]
}


